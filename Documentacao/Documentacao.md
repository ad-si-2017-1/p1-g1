# Projeto de aplicação  - Grupo 1


## Objetivo

Criar um servidor IRC na disciplina de aplicações distribuidas contento os comandos "NICK", "JOIN","USER", as demais funções devem ser apresentadas após o conhecimento básico das funções anteriores.
Cada projeto será feito em um sprint semanal informado por Marcelo Akira toda segunda-feira.

## Integrantes 

* Líder: Anderson;

* Desenvolvedores: Anderson, Silvio;


* Documentadores: Erick Vinicius, Marcos Ariel.

## Introdução

O protocolo IRC é baseado no modelo cliente-servidor e é adequado para executar em muitas máquinas de forma distribuída. Uma configuração típica envolve um único processo (o servidor) formando um ponto central para os clientes (ou outros servidores) para se conectar, executar a mensagem de entrega / multiplexação e outras funções.
Este modelo distribuído, que exige que cada servidor tenha uma cópia da informação de estado global, ainda é o problema mais flagrante do protocolo, pois é uma deficiência grave, que limita o tamanho máximo que uma rede pode alcançar. Se as redes existentes têm sido capazes de continuar crescendo a um ritmo incrível, devemos agradecer aos fabricantes de hardware por nos dar sistemas cada vez mais poderosos.

## Servidores

O servidor forma a espinha dorsal do IRC, pois é o único componente do protocolo que é capaz de ligar todos os outros componentes em conjunto: ele fornece um ponto para que os clientes podem se conectar para conversar uns com os outros, e um ponto Para que outros servidores se conectem. O servidor também é responsável por fornecer os serviços básicos definidos pelo protocolo IRC.

## IRC - Arquitetura básica

A arquitetura básica do IRC, no caso mais simples, existe um único servidor IRC ao qual vários clientes IRC podem se conectar. Um cliente IRC se conecta ao servidor com uma identidade específica. Mais notavelmente, cada cliente deve escolher um apelido exclusivo , ou "nick". Uma vez que um cliente está conectado, ele pode se comunicar um-para-um com outros usuários. Além disso, os clientes podem executar comandos para consultar o estado do servidor (por exemplo, obter uma lista de usuários conectados ou obter detalhes adicionais sobre um nick específico). O IRC também suporta a criação de salas de bate-papo chamadas de canais para comunicação um-para-muitos. Os usuários podem participar de canais e enviar mensagens para o canal.


A imagem abaixo mostra a descrição da arquitetura básica.


![Arquitetura de IRC simples](https://imageshack.com/i/pmwcy0klp)


## IRC - Arquitetura com vários servidores

O IRC também suporta a formação de redes de servidores , onde vários servidores formam uma árvore de conexões para suportar mais clientes e proporcionar maior capacidade. Servidores na mesma rede compartilham informações sobre eventos locais (por exemplo, um novo cliente se conecta, um usuário conectado a um determinado servidor se junta a um canal, etc.) para que todos os servidores tenham uma cópia do mesmo estado global. Neste projeto, consideraremos apenas o caso em que há um único servidor IRC.


![Arquitetura de IRC - Multiplos servidores](http://chi.cs.uchicago.edu/_images/architecture2.png)


## Comunicações IRC

### Efetuando login em servidor IRC

Quando um cliente de IRC se conecta a um servidor de IRC, ele deve primeiro registar a sua ligação. Isto é feito enviando duas mensagens: NICK e USER(mensagens 1 e 2 na figura abaixo). NICK especifica o nick do usuário ( Grupo 1neste caso) e USER fornece informações adicionais sobre o usuário. Mais especificamente, USER especifica o nome de usuário do usuário (Grupo1) e o nome completo do usuário ( ) (não estaremos implementando o segundo e terceiro parâmetros).


![Efetuando login em servidor IRC](https://imageshack.com/i/pmXDoZiOj)


Supondo que você escolheu um nick que ainda não foi feito, o servidor IRC enviará uma RPL_WELCOMEresposta (que é o código atribuído 001). Esta resposta tem os seguintes componentes:

* ad.exemplo.com: O prefixo. Lembre-se de que os prefixos são usados ​​para indicar a origem de uma mensagem. Uma vez que esta resposta se origina no servidor ad.exemplo.com, o prefixo simplesmente inclui esse nome de host. Isso pode parecer redundante, dado que o cliente presumivelmente já sabe que está conectado a esse servidor; No entanto, em redes IRC, uma resposta poderia originar em um servidor diferente do que o cliente está conectado.

* 001: O código numérico para RPL_WELCOME.

* grupo 1: O primeiro parâmetro que, nas mensagens de resposta, deve ser sempre o apelido do usuário para o qual esta resposta se destina.

* Bem vindo a Internet Relay Network erick!erick@exemplo.com

### Conectando a um servidor IRC quando o nick escolhido é tirado

Se um usuário tentar se registrar com um nick que já foi feito, o servidor enviará uma ERR_NICKNAMEINUSEresposta (código 433). Observe como os parâmetros nesta resposta são ligeiramente diferentes:

* *:O primeiro parâmetro deve ser o nick do usuário para o qual esta resposta é destinada. No entanto, uma vez que o utilizador ainda não tem um nick, o carácter asterisco é utilizado em vez disso.

* grupo 1: Na resposta ERR_NICKNAMEINUSE , o segundo parâmetro é o "nick offending" (ou seja, o nick que não poderia ser escolhido, porque já está sendo usado).

* Nickname já está sendo usado: O terceiro parâmetro simplesmente inclui uma descrição legível do erro. Os clientes do IRC tipicamente os imprimirão literalmente.


![Conectando a um servidor IRC quando o nick escolhido é tirado](https://imageshack.com/i/pmsIYjn3p)

### Mensagens entre usuários

Uma vez que vários usuários estão conectados, é possível para eles enviar mensagens uns aos outros, mesmo na ausência de canais. Na verdade, a maior parte da segunda atribuição se concentra na implementação de mensagens entre usuários, enquanto a terceira atribuição se concentrará em adicionar suporte para canais.

Para enviar uma mensagem para um nick específico, um usuário deve enviar um PRIVMSG para o servidor. A figura acima mostra dois usuários, com nicks grupo 1 e rory, trocando três mensagens. Na mensagem 1, o usuário grupo 1 envia uma mensagem para rory. Os parâmetros para PRIVMSG são muito simples: o primeiro parâmetro é o nick do usuário para o qual a mensagem é destinada, eo segundo parâmetro é a própria mensagem.

Quando o servidor recebe essa mensagem, e rory assumindo que há um usuário, ele encaminhará a mensagem para o cliente IRC que está registrado com esse nick. Isso é feito na mensagem 2 e observe como o grupo 1 simplesmente copia literalmente a mensagem 1, mas prefixada com o identificador de cliente completo (caso contrário, o cliente IRC do destinatário não teria idéia de quem era a mensagem). As mensagens 3 e 4 mostram uma troca semelhante, exceto indo de rory para grupo 1, e as mensagens 5 e 6 mostram outra mensagem indo de grupo 1 para rory.

![Mensagens entre usuários](https://imageshack.com/i/porXVbkRp)


### Participar, conversar e sair de um canal


Os usuários conectados a um servidor IRC podem se associar a canais existentes usando a mensagem JOIN . O formato da mensagem em si é bastante simples (seu único parâmetro é o nome do canal que o usuário deseja associar), mas resulta em várias respostas sendo enviadas não apenas para o usuário se juntar ao canal, mas também para todos os usuários atualmente no canal. A figura abaixo mostra o que acontece quando o usuário grupo 1 junta canal #tardis, onde dois usuários ( doctor e river) já estão presentes.

A mensagem 1 do grupo 1 JOIN é enviada ao servidor. Quando esta mensagem é recebida, o servidor a relata para os usuários que já estão no canal ( doctor e river) para torná-los cientes de que há um novo usuário no canal (mensagens 2a e 2b). Observe como é retransmitido, JOIN é prefixado com grupo 1 como identificador de cliente completo. O JOIN também é transmitida de volta para grupo 1, como confirmação de que ela se juntou com sucesso ao canal.

As seguintes mensagens (3, 4a, e 4b) fornecem para grupo 1 informações sobre o canal. A mensagem 3 é uma resposta RPL_TOPIC , fornecendo o tópico do canal (esta é uma descrição do canal que pode ser definida por determinados usuários. As mensagens 4a e 4b são respostas RPL_NAMREPLY e RPL_ENDOFNAMES , respectivamente, que indicam ao grupo 1 quais os utilizadores que estão atualmente presentes no canal. Observe como o usuário doctor tem um @ antes de seu nick; Isso indica que doctor é um operador de canal para o canal #tardis.

![Participar, conversar e sair de um canal](https://imageshack.com/i/poHLVWGRp)



Na imagem abaixo mostra que os usuários podem ter modos que lhes dão privilégios especiais no servidor ou em canais individuais.


![privilegios especiais](https://imageshack.com/i/pmYIzRcBp)


Uma vez que um utilizador tenha aderido a um canal, enviar uma mensagem para o canal é essencialmente o mesmo que enviar uma mensagem para um utilizador individual. A diferença é que o servidor retransmitirá a mensagem para todos os usuários no canal, em vez de apenas um único usuário. A figura acima mostra duas mensagens sendo enviadas para o canal #tardis. Primeiro, o usuário doctor envia uma mensagem PRIVMSG, especificando o canal como o alvo (e não um nick, como vimos em "Mensagens entre usuários"). O servidor então retransmite esta mensagem para river e grupo 1, prefixando a mensagem com doctor como identificador de cliente completo (mensagens 1, 2a e 2b). Da mesma forma, grupo 1 envia uma mensagem para o canal, que é retransmitido para doctor e river, com o prefixo do grupo 1 o identificador de cliente está completo (mensagens 3, 4a e 4b).

### Saindo do chat

Sair de um canal é realizado com a mensagem PART, que segue um padrão semelhante para se juntar e falar no canal: o usuário que deseja sair envia uma mensagem PART, e esta mensagem é retransmitida a todos no canal para que eles estão cientes de que o usuário deixou . O servidor também remove internamente esse cliente do canal, o que significa que ele não receberá mais mensagens dirigidas a esse canal. A figura abaixo mostra um exemplo de como isso seria. Observe como a mensagem PART inclui dois parâmetros: o canal que os usuários querem deixar e uma "mensagem de partição" (que é retransmitida como parte da mensagem PART para todos os usuários no canal).

![saindo do chat](https://imageshack.com/i/pnGWijpZj)


## Criando servidor

    net.createServer(function (socket) {
        socket.name = socket.remoteAddress + ":" + socket.remotePort; //É atribuído um nome baseado no ip remoto e na porta que de requisição
        socket.channel = '';// É definido como vazio '' o canal, logo após a entrada do cliente no servidor
        clients.push(socket);//O objeto socket é adicionado na lista de clientes
        var inicioTempo = dataString();// Variável para controlar o tempo de execução do servidor.
    
        //Mensagem de conectado
        socket.write("\n---------------------\n");
        socket.write("CHAT IRC VERSÃO 0.0001\n");
        socket.write("---------------------\n");
        socket.write(" Você está conectado \n");  
  
        //Recebe a entrada dos dados e analisa para validar e identificar o comando/ mensagem    
    socket.on('data', function (data) {
        var comando = data.toString("utf-8").trim().split(' '); //variavel que recebe o texto data  converte em string em seguida se transforma em array de pequenas strings
        var cmd = comando[0].toUpperCase();      

## Comandos

Foi utilizado o "switch" para verificar os camandos no caso do "JOIN, LIST, NICK, PRIVMSG, QUIT, TIME, USER, USERIP".

    switch (cmd) {
 
Caso não seja nenhum comando e for uma mensagem, irá enviar a mensagem e sair do "switch".


    default: //Caso nao ser um comando envia para todos uma mensagem
          broadcastchannel(socket.name.split("|")[1] + " >> " + data.toString("utf-8"), socket);
    break;
    } //end swicth

### JOIN


Este comando é utilizado para incluir algum cliente no canal.

    case "JOIN":
            if (channels.indexOf(comando[1])>=0){
              socket.channel = comando[1].trim();
              socket.write("Você entrou no channel " + comando[1] + "\n");
              broadcast(socket.name.split("|")[1] + " se juntou ao chat.\n", socket);
            }
            else {
              socket.write(erros(475));
            } 
            break;


### LIST

Envia uma lista de canais no servidor.


    case "LIST":
          var channel = '';
          for (i = 0; i < channels.length; i++) {
           channel += channels[i] + "\n";
          }
          socket.write(channel);
          break;       
          
          
### NICK

Comando utilizado para alterar nick.


    case "NICK":
          //Tenta localizar o <nickname> através da chave [comando[1]]
          if (typeof(nick[comando[1]]) != "undefined"){ 
            socket.write(erros(433));
          }
          else{
            if(!comando[1])
                socket.write(erros(432));
            else {
              try{
                delete nick[socket.name.split("|")[1]];
              }
              finally{
                //Realiza associação do Nick ao Endereço IP No Array
                nick[comando[1]] = socket.remoteAddress; 
                socket.name = socket.remoteAddress + ":" + socket.remotePort;
                socket.name += "|" + comando[1];
                socket.write("Confirmado alteração/inclusão do Nick.\n");
              }
            }
          }
    break; 
          
          
### PRIVMSG

Comando para mandar msg privada. 

    case "PRIVMSG":
          // A Variável to recebe o nome de destinatario':'
          var to = data.toString("utf-8").split(" ")[1];
          // A Variável msg recebe todo o texto digitado apos ':'
          var msg = data.toString("utf-8").split(':')[1]; 
          if (!to) {
            socket.write(411);
            return;
          }
          else if (!msg) {
            socket.write(411);
          return;
          }        
          //Caso não haja erros envia a mensagem
          else sendPriv(socket, socket.name.split("|")[1] + ".....| " + msg,to);
    break;


### QUIT

Comando para se desconectar. Deleta o usuário da array associativa.

    
    case "QUIT":
          var nickname = socket.name.split("|")[1];
          delete nick[socket.name.split("|")[1]];
          clients.splice(clients.indexOf(socket), 1);
          socket.destroy();
          broadcast(nickname + " Saiu do Chat.\n");
    break;   
    

### TIME


Comando que envia lista de channels no servidor.

    case "TIME":
          var meses = [["Janeiro"],["Fevereiro"],["Março"],["Abril"],["Maio"],["Junho"],["Julho"],["Agosto"],["Setembro"],["Outubro"],["Novembro"],["Dezembro"]];
          var dataCompleta = dataString().split(":");
          var agora = dataCompleta[0]+" de "+meses[parseInt(dataCompleta[1])]+" de "+dataCompleta[2]+".\nHoras = "+dataCompleta[3]+":"+dataCompleta[4]+":"+dataCompleta[5]+"\n";
          socket.write(agora);
    break;
    
    
### USER

Comando para receber nicknames dos usuários conectados.
      
    case "USERS": 
          var users = "";
            for(var i in nick) {
            users += i + "\n";
          }
          socket.write(users);      
    break;
    
### USERIP

Comando utilizado para exibir o ip de um usuário.

    case "USERIP": 
          if (typeof(nick[comando[1]]) == "undefined") {
            socket.write("Este usuário não existe.\n");
          }
          else {
            socket.write(nick[comando[1]] + "\n");
          }
    break;    

### Remove cliente quando encerra a conexão socket


    socket.on('end', function () {
        clients.splice(clients.indexOf(socket), 1);
    });

## Funções

Essa função envia mensagem para todos, menos para quem enviou.


    function broadcast(message, sender) { 
        clients.forEach(function (client) {
            if(client.socket == '') return;
            if (client === sender) return;
            client.write(message);
        });
      process.stdout.write(message)
    }
  
  
Específico para o canal - função que envia mensagem para todos, menos para quem enviou.


    function broadcastchannel(message, sender) {
        clients.forEach(function (client) {
            var channel = sender.channel;
            if(client.channel == '') return;
            if (client.channel == channel) client.write(message);
        });
      process.stdout.write(message)
    }
    
    
Função para envio de mensagens privadas.


    function sendPriv(sender, message, user) {
        clients.forEach(function (client) {
            var channel = sender.channel;
            if(client.channel == '') return;
            if ((client.channel == channel) && (client.name.split("|")[1] == user)) client.write(message);
        });
      process.stdout.write(message)
    }



function err(codigo){
var mensagem = '';
    switch(codigo) {
        case 411:
            mensagem = "411: Destinatário usente para Mensagem Privada! [ERR_NORECIPIENT].\n";
            break;
        case 412:
            mensagem = "412: Sem texto para enviar![ERR_NOTEXTTOSEND].\n";
            break;
        case 422:
            mensagem = "422: Comando Inválido/Desconhecido![ERR_UNKNOWNCOMMAND].\n";
            break;
        case 432:
            mensagem = "432: Não foi Informado um Nick! [ERR_ERRONEUSNICKNAME]\n";
            break;
        case 433:
            mensagem = "433: Este usuário já existe! [ERR_NICKNAMEINUSE].\n";
            break;
        case 461:
            mensagem = "461: Comando não executado por falta de Parâmetros! [ERR_NEEDMOREPARAMS].\n";
            break;
        case 475:
            mensagem = "475: O canal informado não existe [ERR_BADCHANNELKEY]\n";
            break;
        default:
            mensagem= " ";
    }
    return mensagem;
}
module.exports = err;
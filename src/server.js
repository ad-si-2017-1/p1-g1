//DEFINIÇÕES E LIBS
port = 6667;
net = require('net');
erros = require('./erros');
dataString = require('./dataString');
//identifyComand = require ('./ServerComands');

var clients = []; //array de  clientes
var nick = {}; //mapa de nicks
var channels = ['#CANAL-AD'];

//FUNCAO PRINCIPAL, QUE CRIA O SERVIDOR
net.createServer(function (socket) {
  socket.name = socket.remoteAddress + ":" + socket.remotePort; //É atribuído um nome baseado no ip remoto e na porta que de requisição
  socket.channel = '';// É definido como vazio '' o canal, logo após a entrada do cliente no servidor
  clients.push(socket);//O objeto socket é adicionado na lista de clientes
  var inicioTempo = dataString();// Variável para controlar o tempo de execução do servidor.
    
  //Mensagem de conectado
    var msgConectado = "\n----------------------\nCHAT IRC VERSÃO 0.0001\n----------------------\nServer UFG-IRC-BR G1\n----------------------\nVocê está conectado!\n";
  socket.write(msgConectado);

  //Recebe a entrada dos dados e analisa para validar e identificar o comando/ mensagem    
  socket.on('data', function (data) {

    var comando = data.toString("utf-8").trim().split(' '); //variavel que recebe o texto data  converte em string em seguida se transforma em array de pequenas strings
    var cmd = comando[0].toUpperCase();           
      
    // Função que lê e idenfica o comando --- não está funcionando, preciso ver com o Gabriel
    //identifyComand(cmd, client(socket));
    switch (cmd) {
        //Comando para incluir o cliente em algum canal obs.tem implentar incluir canal,
        case "JOIN":
            try {
                if (channels.indexOf(comando[1]) >= 0) {
                    socket.channel = comando[1].trim();
                    socket.write("Você entrou no channel " + comando[1] + "\n");
                    broadcast(socket.name.split("|")[1] + " se juntou ao chat.\n", socket);
                }
                else {
                    throw 475;
                }
            }catch(err) {
                socket.write(erros(err));}
            finally {
                break;
            }

        //comando que exibe lista de canais no servidor.
        case "LIST":
            try{
                if(!comando[1]) {
                        var channel = '';
                        for (i = 0; i < channels.length; i++) {
                            channel += channels[i] + "\n";
                            }
                        socket.write(channel);
                     }
                else
                     throw 422;

            }catch(err) {
                socket.write(erros(err));}
            finally {
                break;
            }

        //Comando utilizado para alterar nick
        case "NICK":
            try {
                //Tenta localizar o <nickname> através da chave [comando[1]]
                if (typeof(nick[comando[1]]) != "undefined")
                    throw 433;
                else {
                    if (!comando[1])
                        throw 432;
                    else {
                        delete nick[socket.name.split("|")[1]];

                        //Realiza associação do Nick ao Endereço IP No Array
                        nick[comando[1]] = socket.remoteAddress;
                        socket.name = socket.remoteAddress + ":" + socket.remotePort;
                        socket.name += "|" + comando[1];
                        socket.write("Confirmado alteração/inclusão do Nick.\n");
                    }
                }
            }catch(err) {
                socket.write(erros(err));
            }
            finally {
                    break;
                }


  
        //comando para mandar msg privada. Use: PRIVMSG USER :MESSAGE
        case "PRIVMSG":

            try {
                // A Variável to recebe o nome de destinatario':'
                var to = data.toString("utf-8").split(" ")[1];
                // A Variável msg recebe todo o texto digitado apos ':'
                var msg = data.toString("utf-8").split(':')[1];
                if (!to)
                    throw 461;
                else if (!msg)
                    throw 411;
                else
                    //Caso não haja erros envia a mensagem
                    sendPriv(socket, socket.name.split("|")[1] + ".....| " + msg, to);
                }
            catch(err) {
                socket.write(erros(err));
            }
            finally {
                break;
            }
        //comando para se desconectar. Deleta o usuário da array associativa.   
        case "QUIT":
            try {
                if(!comando[1]) {
                    var nickname = socket.name.split("|")[1];
                    delete nick[socket.name.split("|")[1]];
                    clients.splice(clients.indexOf(socket), 1);
                    socket.destroy();
                    broadcast(nickname + " Saiu do Chat.\n");

                }
                else
                    throw 422;
            }
            catch(err) {
                socket.write(erros(err));
            }
            finally {

                break;
            }
        
        //comando que envia lista de channels no servidor.
        case "TIME":
            try {
                if(!comando[1]) {
                    var meses = [["Janeiro"], ["Fevereiro"], ["Março"], ["Abril"], ["Maio"], ["Junho"], ["Julho"], ["Agosto"], ["Setembro"], ["Outubro"], ["Novembro"], ["Dezembro"]];
                    var dataCompleta = dataString().split(":");
                    var agora = dataCompleta[0] + " de " + meses[parseInt(dataCompleta[1])] + " de " + dataCompleta[2] + " " + dataCompleta[3] + ":" + dataCompleta[4] + ":" + dataCompleta[5] + "\n";
                    socket.write(agora);
                }
                    else
                        throw 422;
            }catch(err) {
                socket.write(erros(err));
            }
            finally {

                    break;
                }


        //comando para receber nicknames dos usuários conectados.      
        case "USERS":
          try {
              if(!comando[1]) {
                      var users = "";
                      for (var i in nick) {
                          users += i + "\n";
                      }
                      socket.write(users);
                }
              else
                  throw 422;
          }catch(err) {
              socket.write(erros(err));}
          finally {
              break;
          }

      
        //Comando utilizado para exibir o ip de um usuário
        case "USERIP":
          try {
              if (typeof(nick[comando[1]]) == "undefined")
                  throw 432;
              else {
                  //var StrIp = nick[comando[1]];
                  var StrIp = nick[comando[1]].split(":")[3];
                  socket.write(StrIp + "\n");

              }

          }
          catch(err) {
              socket.write(erros(err));
          }
          finally {
              break;
          }


        //Caso nao ser um comando envia para todos uma mensagem
        default:
                broadcastchannel(socket.name.split("|")[1] + " ---> " + data.toString("utf-8"), socket);
                break;

    } //final do Switch
  });

  //Remove o cliente quando encerra a conexao com o socket
  socket.on('end', function () {
    clients.splice(clients.indexOf(socket), 1);
  });
  //funcao que envia mensagem para todos menos para quem enviou
  function broadcast(message, sender) { 
      clients.forEach(function (client) {
          if(client.socket == '') return;
          if (client === sender) return;
          client.write(message);
      });
      process.stdout.write(message)
  }
    //(especíco para o canal)funcao que envia mensagem para todos menos para quem enviou
  function broadcastchannel(message, sender) {
      clients.forEach(function (client) {
          var channel = sender.channel;
          if(client.channel == '') return;
          if (client.channel == channel) client.write(message);
      });
      process.stdout.write(message)
  }
  //funcao para envio de mensagens privadas
  function sendPriv(sender, message, user) {
      clients.forEach(function (client) {
          var channel = sender.channel;
          if(client.channel == '') return;
          if ((client.channel == channel) && (client.name.split("|")[1] == user)) client.write(message);
      });
      process.stdout.write(message)
  }
}).listen(port);
console.log("Servidor funcionando na porta " + port + "\n");
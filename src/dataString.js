function dataString() {
    var data = new Date();
    var hora = data.getHours();
    hora = (hora < 10 ? "0" : "") + hora;
    var min  = data.getMinutes();
    min = (min < 10 ? "0" : "") + min;
    var seg  = data.getSeconds();
    seg = (seg < 10 ? "0" : "") + seg;
    var ano = data.getFullYear();
    var mes = data.getMonth() + 1;
    mes = (mes < 10 ? "0" : "") + mes;
    var dia  = data.getDate();
    dia = (dia < 10 ? "0" : "") + dia;
    return dia+":"+mes+":"+ano+":"+hora+":"+min+":"+ seg;
}
module.exports = dataString;
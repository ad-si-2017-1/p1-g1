// MODULO DE COMANDOS DO SERVIDOR
// NÃO ESTÁ PRONTO PRECISA PASSAR O PARAMENTRO DE USUARIO, TIRAR DÚVIDA COM O COM GABRIEL,

function identifyComand(comando) {   
    switch (comando) {
        
        /* IMPLENTAR ESTES NA PRÓXIMA SPRINT:
        case "MODE":
        case "NAMES":
        case "PART":
        case "PONG":
        case "PING":
        case "WHO":
        */

        //Comando para incluir o cliente em algum canal ou criar o  canal
        case "JOIN":
            if (channels.indexOf(comando[1])>=0){
              socket.channel = comando[1].trim();
              socket.write("Você entrou no channel " + comando[1] + "\n");
              broadcast(socket.name.split("|")[1] + " se juntou ao chat.\n", socket);
            }
            else {
              socket.write(erros(475));
            } 
            break;

        //comando que envia lista de channels no servidor.
        case "LIST":
          var channel = '';
          for (i = 0; i < channels.length; i++) {
           channel += channels[i] + "\n";
          }
          socket.write(channel);
          break;            

        //Comando utilizado para alterar nick, conforme RFC DO PROTOCOLO IRC
        case "NICK":
          //Tenta localizar o <nickname> através da chave [comando[1]]
          if (typeof(nick[comando[1]]) != "undefined"){ 
            socket.write(erros(433));
          }
          else{
            if(!comando[1])
                socket.write(erros(432));
            else {
              try{
                delete nick[socket.name.split("|")[1]];
              }
              finally{
                //Realiza associação do Nick ao Endereço IP No Array
                nick[comando[1]] = socket.remoteAddress; 
                socket.name = socket.remoteAddress + ":" + socket.remotePort;
                socket.name += "|" + comando[1];
                socket.write("Confirmado alteração/inclusão do Nick.\n");
              }
            }
          }
          break; 
  
        //comando para mandar msg privada. Use: PRIVMSG USER :MESSAGE
        case "PRIVMSG":
          // A Variável to recebe o nome de destinatario':'
          var to = data.toString("utf-8").split(" ")[1];
          // A Variável msg recebe todo o texto digitado apos ':'
          var msg = data.toString("utf-8").split(':')[1]; 
          if (!to) {
            socket.write(411);
            return;
          }
          else if (!msg) {
            socket.write(411);
          return;
          }        
          //Caso não haja erros envia a mensagem
          else sendPriv(socket, socket.name.split("|")[1] + ".....| " + msg,to);
          break;
        
        //comando para se desconectar. Deleta o usuário da array associativa.   
        case "QUIT":
          var nickname = socket.name.split("|")[1];
          delete nick[socket.name.split("|")[1]];
          clients.splice(clients.indexOf(socket), 1);
          socket.destroy();
          broadcast(nickname + " Saiu do Chat.\n");
          break;    
        
        //comando que envia lista de channels no servidor.
        case "TIME":
          var meses = [["Janeiro"],["Fevereiro"],["Março"],["Abril"],["Maio"],["Junho"],["Julho"],["Agosto"],["Setembro"],["Outubro"],["Novembro"],["Dezembro"]];
          var dataCompleta = dataString().split(":");
          var agora = dataCompleta[0]+" de "+meses[parseInt(dataCompleta[1])]+" de "+dataCompleta[2]+".\nHoras = "+dataCompleta[3]+":"+dataCompleta[4]+":"+dataCompleta[5]+"\n";
          socket.write(agora);
          break;

        //comando para receber nicknames dos usuários conectados.      
        case "USERS":
          var users = "";
            for(var i in nick) {
            users += i + "\n";
          }
          socket.write(users);      
          break;
      
        //Comando utilizado para exibir o ip de um usuário
        case "USERIP":
          if (typeof(nick[comando[1]]) == "undefined") {
            socket.write("Este usuário não existe.\n");
          }
          else {
            socket.write(nick[comando[1]] + "\n");
          }
          break;               

        //Caso nao ser um comando envia para todos uma mensagem
        default:
          broadcastchannel(socket.name.split("|")[1] + "....." + data.toString("utf-8"), socket);
          break;
    } //end swicth
}
module.exports = identifyComand;
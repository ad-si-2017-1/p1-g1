# PROJETO 1 - APLICAÇÕES DISTRIBUÍDAS - GRUPO 1

MEMBROS:
* ANDERSON MARTINS DIAS - LÍDER
* ERICK VINICIUS OLIVEIRA CRUZ DAHER
* MARCOS ARIEL RODRIGUES DE JESUS
* SILVIO PASSOS

## INFORMAÇÕES IMPORTANTES:
    
        I.  Este projeto é uma das atividades desenvolvidas na disciplina de Aplicações Distribuídas-UFG;
    
        II. Para aprovação nesta Disciplina, se faz necessária a colaboração entre os participantes do grupo;

        III.Para facilitar o trabalho a equipe foi dividida, dois integrantes ficaram responsáveis pelo codificação
            e os outros dois responsáveis por produzir a documentação.


### TECNOLOGIAS UTILIZADAS:

        - [Máquina Virtual - Debian]- Disponibilizada pelo o professor no moodle
	    - [Sistemas Operacionais] Linux Ubuntu e Windows
	    - [Editores de Texto - IDE] Gedit, Nano, Vim, Sublime Text, Notepad, Notepad++, IntelliJ IDEA
	    - [Standard ECMA-262 ] Documentação da Linguagem Javascript
        - [Sockets]- Material disponibilizado no moodle
        - [Nodes.js] - Ferramenta de apoio - (Servidor)
        - [NPM]- Ferramenta de apoio - (Intalador de pacotes)
        - [Javascript-js] Linguagem de Programação
        - [GitLab] - Ferramenta de apoio e controle de versão
        - [RFC- 2812-2813] - Documentação do Protocolo IRC
    

### INSTALAÇÃO:

    -[Nodes.js] (https://nodejs.org/en/) v.7.8 - Downloads
    
        * Instale as dependências e inicie o servidor 
    
    -[Chatzilla] add-on IRC do Firefox

        * Depois de instalar, entrar no menu: Tools > Chatzilla

        * Para entrar em um canal, acessar no menu: Join Channel, escolher o 
        servidorfreenode e o canal, por exemplo "#CANAL-AD"

        * Caso queira entrar em um canal ou criar um novo, use o comando:

            JOIN #CANAL-AD

        * Para efetuar a troca ou inclusão do nick:

             NICK <novonickname>
             
        * Para listar os canais do servidor:

             LIST
             
        * Para enviar uma mensagem privada para um determinado usuário:

             PRIVMSG <user>:mensagem
        
        * Para ver o horário local do servidor:

             TIME
        
        * Para listar os usuários conectados no servidor:

             USERS
        
        * Para exibir o ip de um determinado usuário:

             USERIP

   * Procure por comandos IRC:
   <http://www.prof2000.pt/users/lpitta/ajuda/mirc/lista_cmd_mirc.htm>
   <http://www.pthelp.org/tutorial_comandos>
            
  * Link contendo as mensagens do servidor IRC:
  <https://gist.github.com/Ferus/2632945>
        
  * Link contendo as mensagens de erro e seus respectivos codigos, para servidor IRC:
  <http://www2.irchelp.org/irchelp/rfc/chapter6.html>
  <https://www.alien.net.au/irc/irc2numerics.html>

### DESENVOLVIMENTO:

    >Sprint 1
	- Desenvolvimento do Servidor
	- Tradução dos RFCs
	- Início da Produção da Documentação

    >Sprint 2
	- Modularização do Código
	- Aprimoramento da Documentação
	
    >Sprint 3
	- Remoção dos Módulos devido incompatibilidades e conflitos.
	- Reinicio do Projeto de Codificação do Servidor
	- Modularizaçao de algumas funcionalidades
	- Tratamento de Erros para Cada Comando
	- Detalhamento da Documentação
	
 
